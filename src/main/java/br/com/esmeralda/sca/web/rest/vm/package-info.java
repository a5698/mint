/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.esmeralda.sca.web.rest.vm;
